![NORD University](https://study-eu.s3.amazonaws.com/uploads/university/nord-university-151-logo.png)

**Game Lab 3 - Team Tasktrolls**


*Currently under development using Unity 2019.2.3f1*


**Instructions**
1.  Please clone this repository using your Sourcetree client. (Can be easily cloned straight from your Remote repositories page).
2.  Once cloned into your desired folder directory on your computer. Open your Unity Hub program, then Add the project "GL3_FlowingSilver" from this repository.
3.  Open the "GL3_FlowingSilver" project straight from your Unity Hub.
3.  Make sure to always use 2019.2.3f1 until we in the team decide on upgrading to newer future versions.


| Playable gameprotytype builds |  |
| ------ | ------ |
| WebGL (Play directly in your browser) | https://tasktrolls.itch.io/flowing-silver |
| Windows (Download) | Coming soon... | 
| Mac (Download) | Coming soon... | 
